# Role: telegraf
This role install and setup telegraf daemon to gather and send metrics to influxdb or other timeseries database.

Configuration example:
```
telegraf_influxdb_urls:
    - http://influxdb.mycompany.org:8086
```