# USES
```
sudoers:
  - name: unixadm_l
    ensure: present
    comment: "Local administrators policy"
    groups:
      - unixadm_l
    hosts:
      - ALL
    runas:
      - root
    cmnds:
      - ALL
    tags:
      - NOPASSWD
    defaults:
      - 'env_keep += "SSH_AUTH_SOCK"'
  - name: nixadm
    ensure: present
    comment: "AD administrators policy"
    groups:
      - nixadm
    hosts:
      - ALL
    runas:
      - root
    cmnds:
      - ALL
    tags:
      - NOPASSWD
    defaults:
      - 'env_keep += "SSH_AUTH_SOCK"'
```
