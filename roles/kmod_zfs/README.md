# Role: kmod_scst
This role install ZFS module and userspace software / tools and set options for ZFS kernel module.
Please note, currently all module options define in j2 template and has no special structure in this role.