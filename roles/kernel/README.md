# Role: kernel
This role is used to setup core kernel parameters but not the parameters of specific module, for example zfs / spl. 
If you want to override boot parametes please use __kernel_boot_parameters__ variable for this purpose.
If you want to override kernel parameters pelase use __kernel_parameters__ variable.

__Note__: __kernel_boot_parameters__ apply only after system reboot.