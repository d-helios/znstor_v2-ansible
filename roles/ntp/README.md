# Role: ntp

Install and configure ntp service. Use __ntp_servers__ variable to define list of ntp servers.

Example: 
```
all:
  children:
    cluster10:
      vars:
        ntp_servers:
          - time.my.company.com
      hosts:
        cluster10-n1:
        cluster10-n2:
```