# Role: pacemaker
This role install and setup HA-Cluster for ZNStor.

To set primary and secondary redundency ring interfaces, please use following variables:
```
corosync_bindnet_interface_1
corosync_bindnet_interface_2
```

__Note__: This role does not configure any cluster resources or setup stonith, but keep in mind corosync configuration is optimized only for two node cluster environment.