# znstor v2
This playbook deploy znstor_v2 cluster.


Inventory example:
```bash
all:
  children:
    cluster10:
      vars:
        ntp_servers:
          - time.mycompany.com
        telegraf_influxdb_urls:
          - http://influxdb.mycompany.com:8086
        telegraf_influxdb_database: znstor_v2
      hosts:
        cluster10-n1:
        cluster10-n2:
```
